import sqlite3

# ========= Database / Setup ===========

connection = sqlite3.connect('users_data.db')
cursor = connection.cursor()

# ========== Queries ===============

command = """CREATE TABLE IF NOT EXISTS users(id INTEGER AUTO_INCREMENT PRIMARY KEY, username TEST, password TEXT)"""

cursor.execute(command)