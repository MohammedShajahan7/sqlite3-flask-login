import sqlite3


connection = sqlite3.connect('users_data.db')
cursor = connection.cursor()

# ============= Queries ==================

cursor.execute("INSERT INTO users VALUES ('1', 'mohammed', 'qwerty123')")
cursor.execute("INSERT INTO users VALUES ('2', 'mohammedsh', 'mohammedsh123')")
cursor.execute("INSERT INTO users VALUES ('3', 'Miller', 'miller123')")

connection.commit()